﻿using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeWelding
{
	public class FeedbackConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
				return null;

			var boolValue = (bool)value;

			return boolValue ? 1 : 0.2;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

