using System;
using System.Globalization;

using Xamarin.Forms;

namespace LindeWelding
{
	public class GasSelectedConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is bool))
				return Color.FromHex("#005493");

			var boolValue = (bool)value;

			return boolValue ? Color.FromHex("#81aac6") : Color.FromHex("#005493");
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return false;
			//throw new NotImplementedException();
		}
	}
}

