using System;

namespace LindeWelding
{
	public interface IUserSettings
	{
		void Save(string key, object value);
		bool RestoreBool(string key);
		int RestoreInt(string key);
	}
}
