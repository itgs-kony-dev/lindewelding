using System;
namespace LindeWelding
{
	public interface ICombination
	{
		string Metal { get; set; }
		string Process { get; set; }
		string Gas { get; set; }
	}
}

