using System;
using System.ComponentModel;

namespace LindeWelding
{
	public class ModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			try
			{
				PropertyChangedEventHandler handler = PropertyChanged;

				if (handler != null)
					handler(this, new PropertyChangedEventArgs(propertyName));
			}
			catch (Exception exception)
			{
				System.Diagnostics.Debug.WriteLine("Exception: " + exception.Message);
			}
		}

		public ModelBase()
		{
		}
	}
}