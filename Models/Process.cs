using System;

using Newtonsoft.Json;

namespace LindeWelding
{
	public class Process : IIdentifiable
	{
		[JsonProperty("ID")]
		public string Id { get; set; }

		[JsonProperty("Name")]
		public string Name { get; set; }

		public override string ToString()
		{
			return Name.ToString();
		}
	}
}

