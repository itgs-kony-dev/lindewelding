﻿using System;

using Newtonsoft.Json;

namespace LindeWelding
{
	public class TodoItem : IIdentifiable
	{
		public TodoItem()
		{
		}

		[JsonProperty("ID")]
		public string Id { get; set; }
		public string Text { get; set; }

	}
}
