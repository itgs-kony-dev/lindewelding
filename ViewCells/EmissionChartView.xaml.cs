﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace LindeWelding
{
	public partial class EmissionChartView : ContentView
	{
		public static readonly BindableProperty ValueProperty = BindableProperty.Create("Rate", typeof(double), typeof(EmissionChartView), 0d, propertyChanging: (x, o ,n) => ((EmissionChartView)x).OnValueChanged(x,n,o));
		public static readonly BindableProperty MaxProperty = BindableProperty.Create("Max", typeof(double), typeof(EmissionChartView), 0d, propertyChanging: (x, o, n) => ((EmissionChartView)x).OnMaxChanged(x, n, o));

		public double Value
		{
			get { return (double)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		public double Max
		{
			get { return (double)GetValue(MaxProperty); }
			set { SetValue(MaxProperty, value); }
		}

		public EmissionChartView()
		{
			InitializeComponent();

			this.SetBinding(ValueProperty, new Binding("Value", BindingMode.TwoWay));
			this.SetBinding(ValueProperty, new Binding("Max", BindingMode.TwoWay));
		}

		protected void OnValueChanged(BindableObject bindable, object newValue, object oldValue)
		{
			double dValue = (double) newValue;

			double oldHeight = this.chartBoxView.HeightRequest;
			double newHeight = Max > 0 ? (Max <= double.Epsilon ? 0 : (dValue / Max) * 110) : 0;

			Animation animation = new Animation(v => this.chartBoxView.HeightRequest = v, oldHeight, newHeight);
			animation.Commit(this, "SimpleAnimation", 50, 500, Easing.Linear, null, () => false);

			this.valueLabel.Text = (dValue <= double.Epsilon ? "" : String.Format("{0:0.00} mL/min", dValue));
		}

		protected void OnMaxChanged(BindableObject bindable, object newValue, object oldValue)
		{
			double dValue = (double)newValue;

			double oldHeight = this.chartBoxView.HeightRequest;
			double newHeight = dValue > 0 ? (dValue <= double.Epsilon ? 0 : (Value / dValue) * 110) : 0;

			Animation animation = new Animation(v => this.chartBoxView.HeightRequest = v, oldHeight, newHeight);
			animation.Commit(this, "SimpleAnimation", 50, 500, Easing.Linear, null, () => false);
			this.maxLabel.Text = (dValue <= double.Epsilon ? "" : String.Format("{0:0.0}", dValue));

		}
	}
}
