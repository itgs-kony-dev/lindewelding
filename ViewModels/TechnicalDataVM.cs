﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Input;
using System.Collections.ObjectModel;
using Microsoft.WindowsAzure.MobileServices.Files;

//using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage; 
using Microsoft.WindowsAzure.Storage.Blob; 

using Xamarin.Forms;
using System.IO;

namespace LindeWelding
{
	public enum CorgonMison
	{
		both = 0,
		Corgon = 1,
		Mison = 2,
	}

	public enum Attributes
	{
		none = 0,
		WeldingSpeed = 1,
		PorosityControl = 2,
		Fusion = 3,
		Penetration = 4,
		EaseOfUse = 5,
		ThicknessRange = 6 
	}

	public class TechnicalDataVM : ModelBase
	{
		private readonly IFileHelper fileHelper;

		public readonly MatProcVM aluminiumVM;
		public readonly MatProcVM mildSteelVM;
		public readonly MatProcVM stainlessSteelVM;
		public readonly MatProcVM titaniumVM;
		public readonly MatProcVM cooperVM;

		public readonly Process GMAW;
		public readonly Process GTAW;

		public ObservableCollection<Gas> Gases { get; private set; }
		public ObservableCollection<Gas> Gases2 { get; private set; }
		public ObservableCollection<TechnicalData> TechnicalData { get; private set; }
		public ObservableCollection<Emission> Emissions { get; private set; }

		IEnumerable<ICombination> combos;

		double ffr;
		double ffr2;

		bool options;

		Metal material;
		Process process;
		CorgonMison corgonMison;
		string em;

		ImageSource crossSection;
		ImageSource crossSection2;

		ImageSource surface;
		ImageSource surface2;

		VideoView2 video;
		VideoView2 video2;

		ImageSource preview3D;
		ImageSource preview3D2;

		Attributes sortAttribute;

		public Attributes SortAttribute
		{ 
			get { return this.sortAttribute; }
			set { this.sortAttribute = value; Initialize(); }
		}

		public double FFR2
		{
			set
			{
				this.ffr2 = value;
				OnPropertyChanged("FFR2");
			}
			get { return this.ffr2; }
		}

		public double FFR
		{
			set
			{
				this.ffr = value;
				OnPropertyChanged("FFR");
			}
			get { return this.ffr; }
		}

		public string Emission
		{
			set
			{
				this.em = value;
				OnPropertyChanged("Emission");

				if (Material != null && Process != null)
					Initialize();
			}
			get { return this.em; }
		}

		public Metal Material
		{
			set
			{
				this.material = value;
				OnPropertyChanged("Material");

				if (Material != null && Process != null)
					Initialize();
				else
					Unselect();
			}
			get { return this.material; }
		}

		public Process Process
		{
			set
			{
				this.process = value;
				OnPropertyChanged("Process");

				if (Material != null && Process != null)
					Initialize();
				else
					Unselect();
			}
			get { return this.process; }
		}

		public CorgonMison CorgonMison
		{
			set
			{
				this.corgonMison = value;
				OnPropertyChanged("CorgonMison");

				if (Material != null && Process != null)
					Initialize();
				else
					Unselect();
			}
			get { return this.corgonMison; }
		}

		public ImageSource CrossSection
		{
			set
			{
				this.crossSection = value;
				OnPropertyChanged("CrossSection");
			}
			get { return this.crossSection; }
		}

		public ImageSource CrossSection2
		{
			set
			{
				this.crossSection2 = value;
				OnPropertyChanged("CrossSection2");
			}
			get { return this.crossSection2; }
		}

		public ImageSource Surface
		{
			set
			{
				this.surface = value;
				OnPropertyChanged("Surface");
			}
			get { return this.surface; }
		}

		public ImageSource Surface2
		{
			set
			{
				this.surface2 = value;
				OnPropertyChanged("Surface2");
			}
			get { return this.surface2; }
		}

		public VideoView2 Video
		{
			set
			{
				this.video = value;
				OnPropertyChanged("Video");
			}
			get { return this.video; }
		}

		public VideoView2 Video2
		{
			set
			{
				this.video2 = value;
				OnPropertyChanged("Video2");
			}
			get { return this.video2; }
		}

		public ImageSource Preview3D
		{
			set
			{
				this.preview3D = value;
				OnPropertyChanged("Preview3D");
			}
			get { return this.preview3D; }
		}

		public ImageSource Preview3D2
		{
			set
			{
				this.preview3D2 = value;
				OnPropertyChanged("Preview3D2");
			}
			get { return this.preview3D2; }
		}

		public bool Options
		{
			set
			{
				this.options = value;
				OnPropertyChanged("Options");
			}
			get { return this.options; }
		}

		public Gas SelectedGas
		{
			get
			{
				if (Gases != null)
					foreach (Gas gas in Gases)
						if (gas.IsSelected)
							return gas;
				return null;
			}
		}

		public Gas SelectedGas2
		{
			get
			{
				if (Gases2 != null)
					foreach (Gas gas in Gases2)
						if (gas.IsSelected)
							return gas;
				return null;
			}
		}

		public ICommand On3DViewTapped { protected set; get; }

		public ICommand On3DView2Tapped { protected set; get; }

		public TechnicalDataVM(Metal material = null, Process process = null, string emission = "O3")
		{
			aluminiumVM = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Aluminium") };
			aluminiumVM.MaterialSelected = Material?.Name == "Aluminium";
			aluminiumVM.GMAWSelected = Material?.Name == "Aluminium" && App.tdVM.Process?.Name == "GMAW";
			aluminiumVM.GTAWSelected = Material?.Name == "Aluminium" && App.tdVM.Process?.Name == "GTAW";
			aluminiumVM.OnSelected += this.OnSelectionChanged;

			mildSteelVM = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Mild steel") };
			mildSteelVM.MaterialSelected = Material?.Name == "Mild steel";
			mildSteelVM.GMAWSelected = Material?.Name == "Mild steel" && App.tdVM.Process?.Name == "GMAW";
			mildSteelVM.GTAWSelected = Material?.Name == "Mild steel" && App.tdVM.Process?.Name == "GTAW";
			//this.mildSteelMatProcMV.HasCorgon = true;
			mildSteelVM.OnSelected += this.OnSelectionChanged;

			stainlessSteelVM = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Stainless steel") };
			stainlessSteelVM.MaterialSelected = Material?.Name == "Stainless steel";
			stainlessSteelVM.GMAWSelected = Material?.Name == "Stainless steel" && App.tdVM.Process?.Name == "GMAW";
			stainlessSteelVM.GTAWSelected = Material?.Name == "Stainless steel" && App.tdVM.Process?.Name == "GTAW";
			stainlessSteelVM.OnSelected += this.OnSelectionChanged;

			titaniumVM = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Titanium") };
			titaniumVM.MaterialSelected = Material?.Name == "Titanium";
			titaniumVM.GMAWSelected = Material?.Name == "Titanium" && App.tdVM.Process?.Name == "GMAW";
			titaniumVM.GTAWSelected = Material?.Name == "Titanium" && App.tdVM.Process?.Name == "GTAW";
			titaniumVM.OnSelected += this.OnSelectionChanged;

			cooperVM = new MatProcVM { Material = App.Metals.FirstOrDefault(x => x.Name == "Cooper") };
			cooperVM.MaterialSelected = Material?.Name == "Cooper";
			cooperVM.GMAWSelected = Material?.Name == "Cooper" && App.tdVM.Process?.Name == "GMAW";
			cooperVM.GTAWSelected = Material?.Name == "Cooper" && App.tdVM.Process?.Name == "GTAW";
			cooperVM.OnSelected += this.OnSelectionChanged;

			GMAW = App.Processes.FirstOrDefault(x => x.Name == "GMAW");
			GTAW = App.Processes.FirstOrDefault(x => x.Name == "GTAW");

			this.fileHelper = DependencyService.Get<IFileHelper>();

			Material = material;
			Process = process;
			Emission = emission;

			this.Unselect();

			if(Material != null && Process != null)
				Initialize();
		}

		protected void Initialize()
		{
			if (this.material == null && this.process == null)
				return; 
			
			if (Gases != null)
				foreach (Gas gas in Gases)
					if (gas != null)
						gas.IsSelected = false;

			if (Gases2 != null)
				foreach (Gas gas in Gases2)
					if (gas != null)
						gas.IsSelected = false;

			combos = App.Emissions;
			combos = combos.Union(App.TechnicalData);
			combos = combos.Union(App.CrossSections);
			combos = combos.Union(App.WikiData);

			var currentGases2 = combos?
				.Where(x => x.Metal == material.Name)
				.Where(x => x.Process == process.Name)
				.GroupBy(x => x.Gas)
				.Select(x => x.First())
				.Select(x => x.Gas)

				.Join(
					App.TechnicalData
						.Where(x => x.Metal == material.Name)
						.Where(x => x.Process == process.Name),
					combo => combo,
					td => td.Gas,
					(combo, td) => td
				);


			switch (SortAttribute)
			{
				case Attributes.WeldingSpeed:
					currentGases2 = currentGases2.OrderByDescending(x => x.WeldingSpeed);
					break;
				case Attributes.Fusion:
					currentGases2 = currentGases2.OrderByDescending(x => x.Fusion);
					break;
				case Attributes.PorosityControl:
					currentGases2 = currentGases2.OrderByDescending(x => x.PorosityControl);
					break;
				case Attributes.Penetration:
					currentGases2 = currentGases2.OrderByDescending(x => x.Penetration);
					break;
				case Attributes.ThicknessRange:
					currentGases2 = currentGases2.OrderByDescending(x => x.ThicknessRangeTo);
					break;
				case Attributes.EaseOfUse:
					currentGases2 = currentGases2.OrderByDescending(x => x.EaseOfUse);
					break;
			}

			IEnumerable<Gas> currentGases = currentGases2
				.Join(
					App.Gases,
					combo => combo.Gas,
					proc => proc.Name,
					(combo, gas) => gas
				)
				.ToList();

			if (corgonMison != 0)
			{
				currentGases = currentGases.Where(x => x.Name.StartsWith(corgonMison.ToString().ToUpper(), StringComparison.CurrentCulture));
			}
			
			IEnumerable<TechnicalData> technicalData = App.TechnicalData
											 .Where(x => x.Metal == material.Name && x.Process == process.Name);

			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == material.Name && x.Process == process.Name)
									  .ToList().ToArray();
			

			while (currentGases.Count() <= 12)
				currentGases = currentGases.Concat(new List<Gas> {new Gas() });

			Gases = new ObservableCollection<Gas>(currentGases);
			Gases2 = new ObservableCollection<Gas>();

			TechnicalData = new ObservableCollection<TechnicalData>();
			Emissions = new ObservableCollection<Emission>();

			foreach (var currGas in currentGases)
			{
				var x = emissions
					.FirstOrDefault(e => e.Gas == currGas.Name && e.Emission_type == Emission);
				if (x != null && Emission != null)
				{
					System.Diagnostics.Debug.WriteLine("Adding emission " + x.emission + " for gas " + currGas.Name + " and emission param " + Emission);
				}
				Emissions.Add(x);
			}

			foreach (Gas gas in Gases)
			{
				TechnicalData.Add(technicalData
				                  .FirstOrDefault(td => td.Gas == gas.Name));

				//Emissions.Add(emissions
				//			  .FirstOrDefault(e => e.Gas == gas.Name && e.Emission_type == Emission));

				gas.OnSelected += SelectGases;

				Gas gas2 = new Gas(gas);

				Gases2.Add(gas2);

				gas2.OnSelected += SelectGases2;
			}

			Gases[0].IsSelected = true;
			Gases2[1].IsSelected = true;

			OnPropertyChanged("");
		}

		protected virtual void OnSelectionChanged(object sender, StateEventArgs args)
		{
			MatProcVM selectedMatProcVM = (sender as MatProcVM);

			if (sender != this.aluminiumVM && args.State && this.aluminiumVM.MaterialSelected)
				this.aluminiumVM.Unselect();

			if (sender != this.mildSteelVM && args.State && this.mildSteelVM.MaterialSelected)
				this.mildSteelVM.Unselect();

			if (sender != this.stainlessSteelVM && args.State && this.stainlessSteelVM.MaterialSelected)
				this.stainlessSteelVM.Unselect();

			if (sender != this.titaniumVM && args.State && this.titaniumVM.MaterialSelected)
				this.titaniumVM.Unselect();

			if (sender != this.cooperVM && args.State && this.cooperVM.MaterialSelected)
				this.cooperVM.Unselect();

			if (selectedMatProcVM.MaterialSelected != args.State && args.PropertyName == "MaterialSelected")
			{
				if (args.State)
					selectedMatProcVM.MaterialSelected = true;
				else
				{
					App.tdVM.Material = null;
					App.tdVM.Process = null;
					App.tdVM.CorgonMison = 0;
					selectedMatProcVM.Unselect();
				}
			}

			if (args.PropertyName == "GMAWSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GTAWSelected)
						selectedMatProcVM.GTAWSelected = false;

					/*if (selectedMatProcVM.HasCorgon && !selectedMatProcVM.MisonSelected)
						selectedMatProcVM.CorgonSelected = true;
					else
					{
						selectedMatProcVM.CorgonSelected = false;
						selectedMatProcVM.MisonSelected = false;
					}*/

					App.tdVM.Process = GMAW;

					if (App.TutState < 2)
						Device.BeginInvokeOnMainThread(async () => await LindeGasPage.Instance?.PerformTut2());

				}
				else
				{
					App.tdVM.Process = null;
					App.tdVM.CorgonMison = 0;
					//selectedMatProcVM.MisonSelected = false;
					//selectedMatProcVM.CorgonSelected = false;
				}
			}

			if (args.PropertyName == "GTAWSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GMAWSelected)
						selectedMatProcVM.GMAWSelected = false;

					/*if (selectedMatProcVM.CorgonSelected)
						selectedMatProcVM.CorgonSelected = false;

					if (selectedMatProcVM.MisonSelected)
						selectedMatProcVM.MisonSelected = false;
					*/
					App.tdVM.Process = GTAW;
					App.tdVM.CorgonMison = 0;
					//selectedMatProcVM.CorgonSelected = false;
					//selectedMatProcVM.MisonSelected = false;

					if (App.TutState < 2)
						Device.BeginInvokeOnMainThread(async () => await LindeGasPage.Instance?.PerformTut2());
				}
				else
					App.tdVM.Process = null;
			}

			if (args.PropertyName == "CorgonSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GTAWSelected)
						selectedMatProcVM.GTAWSelected = false;

					//if (selectedMatProcVM.MisonSelected)
					//	selectedMatProcVM.MisonSelected = false;

					selectedMatProcVM.GMAWSelected = true;
					//if (selectedMatProcVM.GMAWSelected)
					//	selectedMatProcVM.GMAWSelected = false;

					App.tdVM.Process = GMAW;
					App.tdVM.CorgonMison = CorgonMison.Corgon;
				}
				//else
				//App.tdVM.Process = null;
			}

			if (args.PropertyName == "MisonSelected")
			{
				if (args.State)
				{
					selectedMatProcVM.MaterialSelected = true;

					if (selectedMatProcVM.GTAWSelected)
						selectedMatProcVM.GTAWSelected = false;

					//if (selectedMatProcVM.CorgonSelected)
					//	selectedMatProcVM.CorgonSelected = false;

					selectedMatProcVM.GMAWSelected = true;
					//if (selectedMatProcVM.GMAWSelected)
					//	selectedMatProcVM.GMAWSelected = false;

					App.tdVM.Process = GMAW;
					//App.tdVM.CorgonMison = CorgonMison.Mison;
					//selectedMatProcVM.MisonSelected = true;

					App.tdVM.PreselectIfNot();
				}
				//else
				//App.tdVM.Process = null;
			}
		}

		protected void Unselect()
		{
			Gases = new ObservableCollection<Gas>();
			Gases2 = new ObservableCollection<Gas>();
			Emissions = new ObservableCollection<Emission>();
			TechnicalData = new ObservableCollection<TechnicalData>();

			while (Gases.Count() <= 12)
				Gases.Add(new Gas());

			while (Gases2.Count() <= 12)
				Gases2.Add(new Gas());

			while (Emissions.Count() <= 8)
				Emissions.Add(new Emission());

			while (TechnicalData.Count() <= 8)
				TechnicalData.Add(new TechnicalData() { ThicknessRangeTo = 0 } );

			CrossSection = ImageSource.FromFile("");
			CrossSection2 = ImageSource.FromFile("");
			Surface = ImageSource.FromFile("");
			Surface2 = ImageSource.FromFile("");
			Preview3D = ImageSource.FromFile("");
			Preview3D2 = ImageSource.FromFile("");
			Video = new VideoView2();
			Video2 = new VideoView2();

			FFR = 0;
			FFR2 = 0;

			corgonMison = 0;

			OnPropertyChanged("");
		}

		protected async void SelectGases(object sender, StateEventArgs args)
		{
			foreach (Gas gas in Gases)
			{
				if (gas != sender)
				{
					if (args.State)
					{
						if (gas.IsSelected)
						{
							gas.IsSelected = false;
							//FFR = 0.00;
						}
					}
				}
			}

			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == this.material.Name && x.Process == this.process.Name && x.Emission_type == "FFR")
									  .ToList().ToArray();

			Gas selectedGas = Gases?.FirstOrDefault(g => g.IsSelected);
			Emission emission = emissions?.FirstOrDefault(e => e.Gas == selectedGas?.Name);
			FFR = emission?.emission ?? 0d;

			CrossSection cx = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name && x.Type.ToUpper() == "C");

			CrossSection sf = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name && x.Type.ToUpper() == "S");

			if (cx != null)
			{

				/*Erster Ansatz für FileSync über Azure MobileService und StorageController
				IEnumerable<MobileServiceFile> files = await ItemManager<CrossSection>.Instance.GetImageFiles(cx);

				if (files != null && files.Count() > 0)
				{
					var pic = this.fileHelper.GetLocalFilePath(cx.Id, files.First().Name);

					//CrossSection = ImageSource.FromFile(pic);
					CrossSection = (pic != null) ? ImageSource.FromFile(pic) : null;
				}*/

				//Zweiter Ansatz für FileSync direkt über BlobContainer
				/*CloudBlobClient blobClient = App.storageAccount.CreateCloudBlobClient();
				CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
				CloudBlockBlob blockBlob = container.GetBlockBlobReference("myblob");
				await fileHelper.DownloadToFileAsync(blockBlob, fileHelper.FilePath + "/" + "test.png");

				CrossSection = ImageSource.FromFile(fileHelper.FilePath + "/" + "test.png");*/
			}

			CrossSection = (cx?.Filename != null) ? ImageSource.FromResource(cx.Filename) : null;
			Surface = (sf?.Filename != null) ? ImageSource.FromResource(sf.Filename) : null;

			//IEnumerable<CrossSection> css = await ItemManager<CrossSection>.Instance.GetItemsAsync(true);

			//IEnumerable<MobileServiceFile> files = await ItemManager<CrossSection>.Instance.GetImageFiles(css.Last(x => x.Metal == "Test2"));


			WikiData wikiData = App.WikiData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name);

			Model3DScan model = App.Model3DScan
									  .FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name);

			Preview3D = (model?.Preview != null) ? ImageSource.FromResource(model.Preview) : null;

			if (wikiData != null && wikiData?.Filename != null)
				Video = new VideoView2 { FileName = wikiData?.Filename, WidthRequest = 320, HeightRequest = 320, HorizontalOptions = LayoutOptions.StartAndExpand, VerticalOptions = LayoutOptions.StartAndExpand };
			else
				Video = null;
		}

		protected void SelectGases2(object sender, StateEventArgs args)
		{
			foreach (Gas gas in Gases2)
			{
				if (gas != sender && args.State == true)
					if (gas.IsSelected)
						gas.IsSelected = false;
			}

			Emission[] emissions = App.Emissions
				.Where(x => x.Metal == this.material.Name && x.Process == this.process.Name && x.Emission_type == "FFR")
									  .ToList().ToArray();

			Gas selectedGas = Gases2?.FirstOrDefault(g => g.IsSelected);
			Emission emission = emissions?.FirstOrDefault(e => e.Gas == selectedGas?.Name);
			FFR2 = emission?.emission ?? 0d;

			CrossSection cx = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name && x.Type.ToUpper() == "C");

			CrossSection sf = App.CrossSections
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name && x.Type.ToUpper() == "S");

			CrossSection2 = (cx?.Filename != null) ? ImageSource.FromResource(cx.Filename) : null;
			Surface2 = (sf?.Filename != null) ? ImageSource.FromResource(sf.Filename) : null;

			WikiData wikiData = App.WikiData
				.FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name);

			Model3DScan model = App.Model3DScan
									  .FirstOrDefault(x => x.Metal == this.Material.Name && x.Process == this.Process.Name && x.Gas == selectedGas?.Name);

			Preview3D2 = (model?.Preview != null) ? ImageSource.FromResource(model.Preview) : null;

			if (wikiData != null && wikiData?.Filename != null)
				Video2 = new VideoView2 { FileName = wikiData?.Filename, WidthRequest = 320, HeightRequest = 320, HorizontalOptions = LayoutOptions.StartAndExpand, VerticalOptions = LayoutOptions.StartAndExpand };
			else
				Video2 = null;
		}

		public void PreselectIfNot()
		{
			if (SelectedGas == null)
				Gases[0].IsSelected = true;

			if (SelectedGas2 == null)
				Gases2[1].IsSelected = true;

			OnPropertyChanged("");
		}
	}
}
