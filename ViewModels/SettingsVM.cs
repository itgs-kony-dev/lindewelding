﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Windows.Input;

using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform;

namespace LindeWelding
{
	public class SettingsVM : ModelBase
	{
		public static SettingsVM Instance { get; } = new SettingsVM();

		public Boolean IsLanguageSelected { get; set; }
		public Boolean IsPinSelected { get; set; }
		public Boolean IsFeedbackSelected { get; set; }
		public Boolean IsNotesSelected { get; set; }

		public Boolean IsEnglishSelected { get; set; }
		public Boolean IsGermanSelected { get; set; }
		public Boolean IsSpanishSelected { get; set; }
		public Boolean IsFrenchSelected { get; set; }

		public Boolean Q1Is0Selected { get; set; }
		public Boolean Q1Is1Selected { get; set; }
		public Boolean Q1Is2Selected { get; set; }
		public Boolean Q1Is3Selected { get; set; }
		public Boolean Q1Is4Selected { get; set; }
		public Boolean Q1Is5Selected { get; set; }

		public Boolean Q2Is0Selected { get; set; }
		public Boolean Q2Is1Selected { get; set; }
		public Boolean Q2Is2Selected { get; set; }
		public Boolean Q2Is3Selected { get; set; }
		public Boolean Q2Is4Selected { get; set; }
		public Boolean Q2Is5Selected { get; set; }

		public Boolean Q3Is0Selected { get; set; }
		public Boolean Q3Is1Selected { get; set; }
		public Boolean Q3Is2Selected { get; set; }
		public Boolean Q3Is3Selected { get; set; }
		public Boolean Q3Is4Selected { get; set; }
		public Boolean Q3Is5Selected { get; set; }

		public Boolean Q4Is0Selected { get; set; }
		public Boolean Q4Is1Selected { get; set; }
		public Boolean Q4Is2Selected { get; set; }
		public Boolean Q4Is3Selected { get; set; }
		public Boolean Q4Is4Selected { get; set; }
		public Boolean Q4Is5Selected { get; set; }

		public Boolean Q5Is0Selected { get; set; }
		public Boolean Q5Is1Selected { get; set; }
		public Boolean Q5Is2Selected { get; set; }
		public Boolean Q5Is3Selected { get; set; }
		public Boolean Q5Is4Selected { get; set; }
		public Boolean Q5Is5Selected { get; set; }

		public Boolean Q6Is0Selected { get; set; }
		public Boolean Q6Is1Selected { get; set; }
		public Boolean Q6Is2Selected { get; set; }
		public Boolean Q6Is3Selected { get; set; }
		public Boolean Q6Is4Selected { get; set; }
		public Boolean Q6Is5Selected { get; set; }

		protected SettingsVM()
		{
		}

		public void Select(string selection)
		{
			IsPinSelected = selection == "PIN";
			IsLanguageSelected = selection == "LANGUAGE";
			IsFeedbackSelected = selection == "FEEDBACK";
			IsNotesSelected = selection == "NOTES";

			OnPropertyChanged("");
		}

		public void SelectLanguage(string selection)
		{
			IsEnglishSelected = selection == "en";
			IsGermanSelected = selection == "de";
			IsSpanishSelected = selection == "es";
			IsFrenchSelected = selection == "fr";

			OnPropertyChanged("");
		}

		public void SelectQ1(string selection)
		{
			Q1Is0Selected = selection == "0";
			Q1Is1Selected = selection == "1";
			Q1Is2Selected = selection == "2";
			Q1Is3Selected = selection == "3";
			Q1Is4Selected = selection == "4";
			Q1Is5Selected = selection == "5";

			OnPropertyChanged("");
		}

		public void SelectQ2(string selection)
		{
			Q2Is0Selected = selection == "0";
			Q2Is1Selected = selection == "1";
			Q2Is2Selected = selection == "2";
			Q2Is3Selected = selection == "3";
			Q2Is4Selected = selection == "4";
			Q2Is5Selected = selection == "5";

			OnPropertyChanged("");
		}

		public void SelectQ3(string selection)
		{
			Q3Is0Selected = selection == "0";
			Q3Is1Selected = selection == "1";
			Q3Is2Selected = selection == "2";
			Q3Is3Selected = selection == "3";
			Q3Is4Selected = selection == "4";
			Q3Is5Selected = selection == "5";

			OnPropertyChanged("");
		}

		public void SelectQ4(string selection)
		{
			Q4Is0Selected = selection == "0";
			Q4Is1Selected = selection == "1";
			Q4Is2Selected = selection == "2";
			Q4Is3Selected = selection == "3";
			Q4Is4Selected = selection == "4";
			Q4Is5Selected = selection == "5";

			OnPropertyChanged("");
		}

		public void SelectQ5(string selection)
		{
			Q5Is0Selected = selection == "0";
			Q5Is1Selected = selection == "1";
			Q5Is2Selected = selection == "2";
			Q5Is3Selected = selection == "3";
			Q5Is4Selected = selection == "4";
			Q5Is5Selected = selection == "5";

			OnPropertyChanged("");
		}

		public void SelectQ6(string selection)
		{
			Q6Is0Selected = selection == "0";
			Q6Is1Selected = selection == "1";
			Q6Is2Selected = selection == "2";
			Q6Is3Selected = selection == "3";
			Q6Is4Selected = selection == "4";
			Q6Is5Selected = selection == "5";

			OnPropertyChanged("");
		}
	}
}
