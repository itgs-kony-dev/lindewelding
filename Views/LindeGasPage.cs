﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Linq;
using System.Diagnostics;
using System.Windows.Input;

using Xamarin;
using Xamarin.Forms;

using Plugin.Media.Abstractions;
using Microsoft.WindowsAzure.MobileServices.Files;

using System.Reflection;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net;
using System.IO;

namespace LindeWelding
{
	public partial class LindeGasPage : ContentPage
	{
		public static LindeGasPage Instance { get; private set; }

		string Emission = "O3";

		IFileHelper fileHelper;

		BoxView background = new BoxView { BackgroundColor = Color.FromHex("#80005490") };
		SettingsView settings = new SettingsView();
		Label quit = new Label { Text = "X", TextColor = Color.White, FontSize = 32, FontAttributes = FontAttributes.Bold };

		public LindeGasPage()
		{
			Instance = this;

			InitializeComponent();

			this.fileHelper = this.fileHelper = DependencyService.Get<IFileHelper>();

			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			BindingContext = App.tdVM;

			this.aluminiumView.BindingContext = App.tdVM.aluminiumVM;
			this.mildSteelView.BindingContext = App.tdVM.mildSteelVM;
			this.stainlessSteelView.BindingContext = App.tdVM.stainlessSteelVM;
			this.titaniumView.BindingContext = App.tdVM.titaniumVM;
			this.cooperView.BindingContext = App.tdVM.cooperVM;

			App.TutState = 0;

			if (App.TutState < 1)
			{
				await Task.Delay(1000);

				var b = this.tut01Image.Bounds;
				double oldX = b.X;
				b.X = this.Width / 4;

				await this.tut01Image.LayoutTo(b, 0);
				this.tut01Image.FadeTo(1, 500);
				b.X = oldX;
				await this.tut01Image.LayoutTo(b, 1000, Easing.BounceOut);
			}
		}

		protected virtual void Handle_Clicked(object sender, EventArgs e)
		{
			if (sender is MatProcSelectorView)
			{
				MatProcSelectorView mpsView = (MatProcSelectorView) sender;

				if (mpsView.BindingContext is MatProcVM)
				{
					MatProcVM mpVM = (MatProcVM)mpsView.BindingContext;
					App.tdVM.Material = mpVM.Material;
					App.tdVM.Process = mpVM.GMAWSelected ? App.tdVM.GMAW : (mpVM.GTAWSelected ? App.tdVM.GTAW : null);
				}
			}
		}

		protected void OnSort(object sender, EventArgs args)
		{
			string param = (string) (args as TappedEventArgs).Parameter;
			Debug.WriteLine((args as TappedEventArgs).Parameter);

			if (param == "WeldingSpeed")
				App.tdVM.SortAttribute = Attributes.WeldingSpeed;
			if (param == "PorosityControl")
				App.tdVM.SortAttribute = Attributes.PorosityControl;
			if (param == "Fusion")
				App.tdVM.SortAttribute = Attributes.Fusion;
			if (param == "Penetration")
				App.tdVM.SortAttribute = Attributes.Penetration;
			if (param == "ThicknessRange")
				App.tdVM.SortAttribute = Attributes.ThicknessRange;
			if (param == "EaseOfUse")
				App.tdVM.SortAttribute = Attributes.EaseOfUse;

			App.tdVM.Emission = App.tdVM.Emission;

			this.weldingSpeedImage.Source = 	ImageSource.FromResource(App.tdVM.SortAttribute == Attributes.WeldingSpeed		? "material_select" : "material_unselect");
			this.porosityControlImage.Source = 	ImageSource.FromResource(App.tdVM.SortAttribute == Attributes.PorosityControl	? "material_select" : "material_unselect");
			this.fusionImage.Source = 			ImageSource.FromResource(App.tdVM.SortAttribute == Attributes.Fusion 			? "material_select" : "material_unselect");
			this.penetrationImage.Source = 		ImageSource.FromResource(App.tdVM.SortAttribute == Attributes.Penetration 		? "material_select" : "material_unselect");
			this.thicknessRangeImage.Source = 	ImageSource.FromResource(App.tdVM.SortAttribute == Attributes.ThicknessRange 	? "material_select" : "material_unselect");
			this.easeOfUseImage.Source = 		ImageSource.FromResource(App.tdVM.SortAttribute == Attributes.EaseOfUse 		? "material_select" : "material_unselect");
		}

		protected void OnEmissionPressed(object sender, EventArgs args)
		{

			TappedEventArgs tapArgs = (TappedEventArgs) args;

			App.tdVM.Emission = (string) tapArgs.Parameter;

			this.o3Image.Source = ImageSource.FromResource(App.tdVM.Emission == "O3" ? "emission_select" : "emission_unselect");
			this.noImage.Source = ImageSource.FromResource(App.tdVM.Emission == "NO" ? "emission_select" : "emission_unselect");
			this.no2Image.Source = ImageSource.FromResource(App.tdVM.Emission == "NO2" ? "emission_select" : "emission_unselect");
			this.coImage.Source = ImageSource.FromResource(App.tdVM.Emission == "CO" ? "emission_select" : "emission_unselect");
		}

		protected virtual void OnOverviewTapped(object sender, EventArgs args)
		{
			//Navigation.PushAsync(new OverviewPage());
		}

		public void OnOptionsPressed(object sender, EventArgs args)
		{
			//Navigation.PushAsync(new OverviewPage());
			App.tdVM.Options = !App.tdVM.Options;
		}

		protected async override void OnSizeAllocated(double width, double height)
		{
			base.OnSizeAllocated(width, height);

			await Task.Delay(100);

			Page currentPage = Navigation.NavigationStack.Last();
			Page prevPage = Navigation.NavigationStack[Navigation.NavigationStack.Count-1];

			if(!(currentPage.GetType() == typeof(ComparePage)))
				if (height > width)
				{
					if (App.tdVM.Material != null && App.tdVM.Process != null)
					{
						if (prevPage is ComparePage)
							await Navigation.PopAsync();
						else
							await Navigation.PushAsync(new ComparePage(), false);

					if (App.TutState >= 1)
							this.tut02Image.IsVisible = false;

					}
				}
				else
				{
				}
		}

		protected async void OnTutImageTapped(object sender, EventArgs args)
		{
			Image image = (Image)sender;
			await image.FadeTo(0);
			image.IsVisible = false;

			App.TutState = 1;
			//image.RotateTo(
		}

		public async Task PerformTut2()
		{
			if(App.TutState == 0)
				this.OnTutImageTapped(this.tut01Image, null);
			
			await Task.Delay(1250);

			this.tut02Image.IsVisible = true;
			this.tut02Image.FadeTo(1, 500, Easing.SinIn);
			await this.tut02Image.RotateTo(360, 750);
		}

		protected async void OnTut2ImageTapped(object sender, EventArgs args)
		{
			Image image = (Image)sender;
			await image.FadeTo(0);
			image.IsVisible = false;

			App.TutState = 2;
		}

		void ClearSettingsLayout()
		{
			this.absoluteLayout.Children.Remove(background);
			this.absoluteLayout.Children.Remove(settings);
			this.absoluteLayout.Children.Remove(quit);
		}

		void ClearSettingsLayoutAndSendMail()
		{
			this.absoluteLayout.Children.Remove(background);
			this.absoluteLayout.Children.Remove(settings);
			this.absoluteLayout.Children.Remove(quit);

			string body = "test testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
			string subject = "LindeWelding User Feedback";
			string mailto = "t.hoefkens@itglobal-services.de";

			var url = @"http://cs-srv-dev1.cloudapp.net/middleware/MWServlet?mailto="
				+ WebUtility.UrlEncode(mailto)
							 + "&subject=" + WebUtility.UrlEncode(subject)
							 + "&body= " + WebUtility.UrlEncode(body)
							 + "&appID=OSCAR&serviceID=sendMail&appver=1.0.0&channel=wap&platform=thinclient&cacheid=&";

			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
			request.Method = "POST";
			request.BeginGetResponse(new AsyncCallback((result) =>
			{
				try
				{
					HttpWebRequest myrequest = (HttpWebRequest)result.AsyncState;

					using (HttpWebResponse response = (HttpWebResponse)myrequest.EndGetResponse(result))
					{
						Stream responseStream = response.GetResponseStream();
						using (var reader = new StreamReader(responseStream))
						{
							System.Diagnostics.Debug.WriteLine(reader.ReadToEnd());
						}
						System.Diagnostics.Debug.WriteLine(response.StatusCode);
					}


				}
				catch (Exception e2)
				{
					System.Diagnostics.Debug.WriteLine(e2.Message);
				}
			}), request);

		}

		protected void OnSettingsTapped(object sender, EventArgs e)
		{
			AbsoluteLayout absLayout = null;

			if (Navigation.NavigationStack[Navigation.NavigationStack.Count - 1].GetType() == typeof(LindeGasPage))
				absLayout = LindeGasPage.Instance.AbsLayout;
			if (Navigation.NavigationStack[Navigation.NavigationStack.Count - 1].GetType() == typeof(ComparePage))
				absLayout = ComparePage.Instance.AbsLayout;

			//BoxView background = new BoxView { BackgroundColor = Color.FromHex("#80005490") };
			//SettingsView settings = new SettingsView();
			//Label quit = new Label { Text = "X", TextColor = Color.White, FontSize = 32, FontAttributes = FontAttributes.Bold };

			settings.quitCommand = new Command(ClearSettingsLayoutAndSendMail);

			if (Navigation.NavigationStack[Navigation.NavigationStack.Count - 1].GetType() == typeof(LindeGasPage))
			{
				quit.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command(ClearSettingsLayout)/*new Command((obj) =>
					{
						this.absoluteLayout.Children.Remove(background);
						this.absoluteLayout.Children.Remove(settings);
						this.absoluteLayout.Children.Remove(quit);
					})*/
				});

				this.absoluteLayout.Children.Add(background, new Rectangle(0, 0, 1, 1), AbsoluteLayoutFlags.All);
				this.absoluteLayout.Children.Add(settings, new Rectangle(0.5, 0.5, 0.8, 0.8), AbsoluteLayoutFlags.All);
				this.absoluteLayout.Children.Add(quit, new Rectangle(0.9, 0.045, 64, 64), AbsoluteLayoutFlags.PositionProportional);

			}
			else if (Navigation.NavigationStack[Navigation.NavigationStack.Count - 1].GetType() == typeof(ComparePage)) {
				//Label quit = new Label { Text = "X", TextColor = Color.White, FontSize = 32, FontAttributes = FontAttributes.Bold };
				quit.GestureRecognizers.Add(new TapGestureRecognizer
				{
					Command = new Command((obj) =>
					{
						ComparePage.Instance.AbsLayout.Children.Remove(background);
						ComparePage.Instance.AbsLayout.Children.Remove(settings);
						ComparePage.Instance.AbsLayout.Children.Remove(quit);
					})
				});


				ComparePage.Instance.AbsLayout.Children.Add(background, new Rectangle(0, 0, 1, 1), AbsoluteLayoutFlags.All);
				ComparePage.Instance.AbsLayout.Children.Add(settings, new Rectangle(0.5, 0.5, 0.8, 0.8), AbsoluteLayoutFlags.All);
				ComparePage.Instance.AbsLayout.Children.Add(quit, new Rectangle(0.9, 0.045, 64, 64), AbsoluteLayoutFlags.PositionProportional);
			}
			//absLayout.Children.Add(background, new Rectangle(0, 0, 1, 1), AbsoluteLayoutFlags.All);
			//absLayout.Children.Add(settings, new Rectangle(0.5, 0.5, 0.8, 0.8), AbsoluteLayoutFlags.All);
			//absLayout.Children.Add(quit, new Rectangle(0.9, 0.045, 64, 64), AbsoluteLayoutFlags.PositionProportional);
		}

		protected virtual AbsoluteLayout AbsLayout { get { return this.absoluteLayout; } }
	}
}
