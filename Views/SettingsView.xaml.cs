﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Xamarin.Forms;

namespace LindeWelding
{
	public partial class SettingsView : ContentView
	{
		public SettingsView()
		{
			InitializeComponent();

			BindingContext = SettingsVM.Instance;


		}

		private void OnSettingSelected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.Select(selection);
		}

		private void OnLanguageSelected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectLanguage(selection);
		}

		private void OnQ1Selected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectQ1(selection);
		}

		private void OnQ2Selected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectQ2(selection);
		}

		private void OnQ3Selected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectQ3(selection);
		}

		private void OnQ4Selected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectQ4(selection);
		}

		private void OnQ5Selected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectQ5(selection);
		}

		private void OnQ6Selected(object sender, EventArgs args)
		{
			TappedEventArgs eventArgs = (TappedEventArgs)args;

			System.Diagnostics.Debug.WriteLine(eventArgs.Parameter);

			string selection = (string)eventArgs.Parameter;

			SettingsVM.Instance.SelectQ6(selection);
		}

		public Command quitCommand { get; set; }

		public void ExecQuitCommand(object sender, EventArgs args)
		{
			quitCommand.Execute(0);
		}

		private void OnSendFeedbackTapped(object sender, EventArgs args)
		{
			System.Threading.Tasks.Task.Run(async () =>
			{
				string body = "test";
				string subject = "LindeWelding User Feedback";
				string mailto = "t.hoefkens@itglobal-services.de";

				var url = @"http://cs-srv-dev1.cloudapp.net/middleware/MWServlet?mailto="
					+ WebUtility.UrlEncode(mailto)
								 + "&subject=" + WebUtility.UrlEncode(subject)
								 + "&body= " + WebUtility.UrlEncode(body)
								 + "&appID=OSCAR&serviceID=sendMail&appver=1.0.0&channel=wap&platform=thinclient&cacheid=&";

				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
				request.Method = "POST";
				request.BeginGetResponse(new AsyncCallback((result) =>
				{
					try
					{
						HttpWebRequest myrequest = (HttpWebRequest)result.AsyncState;

						using (HttpWebResponse response = (HttpWebResponse)myrequest.EndGetResponse(result))
						{
							Stream responseStream = response.GetResponseStream();
							using (var reader = new StreamReader(responseStream))
							{
								System.Diagnostics.Debug.WriteLine(reader.ReadToEnd());
							}
							System.Diagnostics.Debug.WriteLine(response.StatusCode);
						}


					}
					catch (Exception e2)
					{
						System.Diagnostics.Debug.WriteLine(e2.Message);
					}
				}), request);


			});

		}	


	}
}
