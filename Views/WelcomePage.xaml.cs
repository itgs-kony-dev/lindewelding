﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LindeWelding
{
	public partial class WelcomePage : ContentPage
	{
		public WelcomePage()
		{
			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			bool isAcknowledged = DependencyService.Get<IUserSettings>().RestoreBool("IsAcknowleded");

			if (!isAcknowledged)
			{
				var answer = await DisplayAlert("CONFIDENTIALITY NOTICE", "This App contains information property of Linde Group that is confidential, privileged and/or exempt from disclosure under applicable law.", "Accept", "Decline");

				if (answer == false)
					throw new Exception();
				else
					DependencyService.Get<IUserSettings>().Save("IsAcknowleded", true);
			}
		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(new Login());
		}
	}
}
