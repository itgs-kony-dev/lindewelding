﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Urho;
using Urho.Forms;

using Xamarin.Forms;

using FormsSample;

namespace LindeWelding
{
	public partial class ComparePage : LindeGasPage
	{
		Charts charts;

		public static ComparePage Instance { get; private set; }

		public ComparePage()
		{
			Instance = this;

			InitializeComponent();

			NavigationPage.SetHasNavigationBar(this, false);


			this.aluminiumView.BindingContext = App.tdVM.aluminiumVM;
			this.mildSteelView.BindingContext = App.tdVM.mildSteelVM;
			this.stainlessSteelView.BindingContext = App.tdVM.stainlessSteelVM;
			this.titaniumView.BindingContext = App.tdVM.titaniumVM;
			this.cooperView.BindingContext = App.tdVM.cooperVM;

			BindingContext = App.tdVM;

			//this.video.Content = new VideoView2 { FileName = "Argon_MildSteel_GTAW.mp4", WidthRequest = 320, HeightRequest = 320, HorizontalOptions = LayoutOptions.StartAndExpand, VerticalOptions = LayoutOptions.StartAndExpand };
		}

		protected override async void OnAppearing()
		{
			base.OnAppearing();

			this.urho3DFrame.IsVisible = false;
			this.urho3DFrame.Opacity = 100;

			if (App.TutState < 3)
			{
				await Task.Delay(1000);

				var b = this.tut03Image.Bounds;
				double oldY = b.Y;
				b.Y = this.Height / 3;

				await this.tut03Image.LayoutTo(b, 0);
				this.tut03Image.FadeTo(1, 500);
				b.Y = oldY;
				await this.tut03Image.LayoutTo(b, 1000, Easing.BounceOut);
			}
		}

		public async void On3DViewTapped(object sender, EventArgs args)
		{
			if (BindingContext is TechnicalDataVM)
			{
				TechnicalDataVM tdVM = (TechnicalDataVM) BindingContext;

				Gas selectedGas = App.tdVM.Gases?.FirstOrDefault(g => g.IsSelected);

				Model3DScan model = App.Model3DScan
										  .FirstOrDefault(x => x.Metal == App.tdVM.Material.Name && x.Process == App.tdVM.Process.Name && x.Gas == selectedGas?.Name);

				this.urho3DFrame.IsVisible = true;

				if (charts == null)
					charts = await urho3DView.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });
				
				charts.SetModel(model.Filename, model.Material);
			}
		}

		public async void On3DView2Tapped(object sender, EventArgs args)
		{
			if (BindingContext is TechnicalDataVM)
			{
				TechnicalDataVM tdVM = (TechnicalDataVM)BindingContext;

				Gas selectedGas = tdVM.Gases2?.FirstOrDefault(g => g.IsSelected);

				Model3DScan model = App.Model3DScan
										  .FirstOrDefault(x => x.Metal == App.tdVM.Material.Name && x.Process == App.tdVM.Process.Name && x.Gas == selectedGas?.Name);

				this.urho3DFrame.IsVisible = true;

				if (charts == null)
					charts = await urho3DView.Show<Charts>(new ApplicationOptions(assetsFolder: null) { Orientation = ApplicationOptions.OrientationType.LandscapeAndPortrait });

				charts.SetModel(model.Filename, model.Material);
			}
		}

		public void OnCrossSectionTapped(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.crossSectionImage.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 64)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnCrossSectionTapped2(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.crossSection2Image.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 64)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnSurfaceTapped(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.surfaceImage.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 0)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnSurfaceTapped2(object sender, EventArgs args)
		{
			Image image = new Image
			{
				Source = this.surface2Image.Source,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Aspect = Aspect.AspectFit,
				Margin = new Thickness(0, 0, 0, 0)
			};

			image.GestureRecognizers.Add(new TapGestureRecognizer
			{
				Command = new Command(async => this.popupView.IsVisible = false)
			});

			this.popupView.Content = image;
			this.popupView.IsVisible = true;
		}

		public void OnVideoViewTapped(object sender, EventArgs args)
		{
			if (BindingContext is TechnicalDataVM)
			{
				TechnicalDataVM tdVM = (TechnicalDataVM)BindingContext;

				Gas selectedGas = tdVM.Gases?.FirstOrDefault(g => g.IsSelected);

				WikiData wikiData = App.WikiData
					.FirstOrDefault(x => x.Metal == App.tdVM.Material.Name && x.Process == App.tdVM.Process.Name && x.Gas == selectedGas?.Name);

				if (wikiData != null && !string.IsNullOrWhiteSpace(wikiData.Filename))
				{
					VideoView videoView = new VideoView { FileName = wikiData.Filename, WidthRequest = 640, HeightRequest = 480, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };

					videoView.GestureRecognizers.Add(new TapGestureRecognizer
					{
						Command = new Command(async => this.popupView.IsVisible = false)
					});

					this.popupView.Content = videoView;
					this.popupView.IsVisible = true;
				}
			}
		}

		public void OnVideoViewTapped2(object sender, EventArgs args)
		{
			if (BindingContext is TechnicalDataVM)
			{
				TechnicalDataVM tdVM = (TechnicalDataVM)BindingContext;

				Gas selectedGas = tdVM.Gases2?.FirstOrDefault(g => g.IsSelected);

				WikiData wikiData = App.WikiData
					.FirstOrDefault(x => x.Metal == App.tdVM.Material.Name && x.Process == App.tdVM.Process.Name && x.Gas == selectedGas?.Name);

				if (wikiData != null && !string.IsNullOrWhiteSpace(wikiData.Filename))
				{
					VideoView videoView = new VideoView { FileName = wikiData.Filename, WidthRequest = 640, HeightRequest = 480, HorizontalOptions = LayoutOptions.CenterAndExpand, VerticalOptions = LayoutOptions.CenterAndExpand };

					videoView.GestureRecognizers.Add(new TapGestureRecognizer
					{
						Command = new Command(async => this.popupView.IsVisible = false)
					});

					this.popupView.Content = videoView;
					this.popupView.IsVisible = true;
				}
			}
		}

		protected override void Handle_Clicked(object sender, EventArgs e)
		{
			if (sender is MatProcSelectorView)
			{
				MatProcSelectorView mpsView = (MatProcSelectorView)sender;

				if (mpsView.BindingContext is MatProcVM)
				{
					MatProcVM mpVM = (MatProcVM)mpsView.BindingContext;
					App.tdVM.Material = mpVM.Material;
					App.tdVM.Process = mpVM.GMAWSelected ? App.tdVM.GMAW : (mpVM.GTAWSelected ? App.tdVM.GTAW : null);
				}
			}
		}

		public void OnClose(object sender, EventArgs args)
		{
			this.popupView.IsVisible = false;
		}

		public void OnCloseUrho(object sender, EventArgs args)
		{
			this.urho3DFrame.IsVisible = false;
			//this.urh
			this.charts.UnsetModel();
			/*this.charts.Dispose();
			this.charts = null;*/
		}

		protected override void OnOverviewTapped(object sender, EventArgs args)
		{
			Navigation.PushAsync(new LindeGasPage());
		}

		protected async override void OnSizeAllocated(double width, double height)
		{
			base.OnSizeAllocated(width, height);

			await Task.Delay(100);

			Page currentPage = Navigation.NavigationStack.Last();
			Page prevPage = Navigation.NavigationStack[Navigation.NavigationStack.Count - 1];

			if(!(currentPage.GetType() == typeof(LindeGasPage)))
				if (height > width)
				{
				}
				else
				{
					if (prevPage is LindeGasPage)
						await Navigation.PopAsync();
					else
						await Navigation.PushAsync(new LindeGasPage(), false);
				}
		}

		protected async void OnTut3ImageTapped(object sender, EventArgs args)
		{
			Image image = (Image)sender;
			await image.FadeTo(0);
			image.IsVisible = false;

			App.TutState = 3;
		}

		protected async Task PerformTut4()
		{
			if (App.TutState < 3)
				this.OnTut3ImageTapped(this.tut03Image, null);

			//Tut4
		}

		protected async void OnGasSelected(object sender, EventArgs args)
		{
			if (App.TutState < 3)
				Device.BeginInvokeOnMainThread(async () => await this.PerformTut4());
		}

		protected override AbsoluteLayout AbsLayout { get { return this.absoluteLayout2; } }

	}
}
