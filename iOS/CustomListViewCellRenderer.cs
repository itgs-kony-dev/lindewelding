using System;

using UIKit;

using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using LindeWelding;

[assembly: ExportRenderer(typeof(ViewCell), typeof(LindeWelding.iOS.CustomListViewCellRenderer))]
namespace LindeWelding.iOS
{
	class CustomListViewCellRenderer : ViewCellRenderer
	{

		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}
	}
}