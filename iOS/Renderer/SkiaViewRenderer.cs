using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof(LindeWelding.SkiaView), typeof(LindeWelding.iOS.SkiaViewRenderer))]

namespace LindeWelding.iOS
{
	public class SkiaViewRenderer: ViewRenderer<SkiaView, NativeSkiaView>
	{
		public SkiaViewRenderer ()
		{
		}

		protected override void OnElementChanged (ElementChangedEventArgs<SkiaView> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				SetNativeControl (new NativeSkiaView (Element));
		}
	}
}

