using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;

using UIKit;
using Contacts;
using Foundation;

using Xamarin;
using Xamarin.Forms;

using LindeWelding;

[assembly: Xamarin.Forms.Dependency(typeof(LindeWelding.iOS.UserSettings))]
namespace LindeWelding.iOS
{
	
	public class UserSettings : IUserSettings
	{
		public UserSettings()
		{
			
		}

		public void Save(string key, object value)
		{
			var plist = NSUserDefaults.StandardUserDefaults;

			if(value is bool)
				plist.SetBool((bool) value, key);
			if (value is int)
				plist.SetInt((int)value, key);
		}

		public bool RestoreBool(string key)
		{
			var plist = NSUserDefaults.StandardUserDefaults;

			return plist.BoolForKey(key);
		}

		public int RestoreInt(string key)
		{
			var plist = NSUserDefaults.StandardUserDefaults;

			return (int) plist.IntForKey(key);
		}
	}
}
